# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import account
from . import party
from . import product


def register():
    Pool.register(
        party.PartyOffice,
        account.Journal,
        account.JournalOffice,
        account.Invoice,
        account.InvoiceLine,
        module='account_office', type_='model')
    Pool.register(
        product.TemplateOffice,
        account.InvoiceLine3,
        module='account_office', type_='model',
        depends=['office_product'])
    Pool.register(
        account.InvoiceLine2,
        module='account_office', type_='model',
        depends=['office_account_product'])
    Pool.register(
        account.InvoiceStandAloneLine,
        module='account_office', type_='wizard',
        depends=['account_invoice_line_standalone_invoice_wizard'])
